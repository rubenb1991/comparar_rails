source 'https://rubygems.org'

gem 'rails', '~> 3.2.13'
gem 'test-unit' # rails necesita test-unit, al menos en consola


gem 'mysql2', '~> 0.3.10'

# gemas de autenticación
gem 'devise'
gem 'declarative_authorization'

gem 'simple_form'
gem 'cocoon'

# group :development do
#   # en mac necesito esta version de libv8
#   gem 'libv8', '>= 3.16.14.7'
# end

# motor de JS usado por less-rails
# gem 'therubyracer'

# gema para poder manejar excepciones de parseo
# (por ahora, solo para WS)
gem 'request_exception_handler'

# Gems used only for assets and not required
# in production environments by default.
group :assets do

  # bootstrap necesita less
  # gem 'less-rails'

  gem 'sass', '~> 3.5.0'
  gem 'sass-rails',   '~> 3.2.6'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby
  gem 'mini_racer'

  gem 'uglifier', '>= 1.0.3'

  # gema para acelerar deploy de assets
  gem 'turbo-sprockets-rails3'
end

# Aunque jquery-rails incluye jquery 3 desde la version 4.2
# (ver https://github.com/rails/jquery-rails/blob/master/VERSIONS.md)
# esa version requiere rails 4.2 o superior.
# Por ahora seguiremos usando jquery manualmente en vendor/assets
# Lamentablemente la gema jquery-rails igual la incluye  la gema jquery-datatables-rails,
# aunque no usaremos jquery-rails por ahora.
# gem 'jquery-rails'

gem 'jquery-ui-rails', '~> 6'

# sería bonito poder usar esta gema, pero requiere ruby 2.3.3
# gem 'bootstrap', '~> 4.5.0'

gem 'awesome_print'

#paginacion
gem 'will_paginate'

# datatable antiguo
gem 'jquery-datatables-rails', '= 1.12.2'

#edicion embebida
gem 'best_in_place', '~> 2.0' #, git: 'git://github.com/johnowenatala/best_in_place'

# Widget para selectores con busqueda in-place
gem 'chosen-rails'

# manejo de archivos
gem 'paperclip'
gem 'aws-sdk', '~> 1'
# en realidad solo ocupamos aws-sdk-s3, pero la version de paperclip no soporta esta nueva version!
# gem 'aws-sdk-s3', '~> 1'

#util para cron
gem 'whenever', :require => false

# editor wysiwyg (texto rico)
# NOTA: ahora la rama tinymce-4 esta en master
gem 'tinymce-rails', '=4.0.12'#, git: 'git://github.com/spohlenz/tinymce-rails.git' #, :branch => "tinymce-4"
# esta no se ocupa, pues estoy usando una traducción personalizada
# gem 'tinymce-rails-langs', git: 'git://github.com/spohlenz/tinymce-rails-langs.git', :branch => "tinymce-4"

# procesos en segundo plano
gem 'sidekiq', '~> 4.0'
gem 'ruby_parser'

# para abrir archivos zip
gem 'rubyzip' #, '~> 1.0.0'

# para imprimir a pdf
gem 'pdfkit'

gem 'wkhtmltopdf-binary', '~> 0.12.3'

# para generar xlsx

# por defecto intenta usar nokogiri 1.6.3, que tiene problemas en mac os...
#gem 'nokogiri', '= 1.6.3.1'
#gem 'htmlentities', '~> 4.3.1'
gem 'axlsx', git: 'git://github.com/randym/axlsx' #, '~> 2.0.1'
gem 'roo'

gem 'puma'

group :development do
  # mejora los mensajes de error en el browser
  gem 'better_errors'
  # usada por la anterior para el stack-trace de los errores (o algo asi)
  gem 'binding_of_caller'
  # assets en consola: por que no te callas!
  gem 'quiet_assets'

  # captura de emails
  gem 'letter_opener'

  # Use unicorn as the app server
  # OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES bundle exec unicorn -c config/unicorn.rb
  # gem 'unicorn'
  #

  # Ruby Raptor como app server:
  # bundle exec passenger start
  # gem "passenger", ">= 5.0.25", require: "phusion_passenger/rack_handler"

  gem 'annotate'

  # Deploy with Capistrano
  gem 'capistrano', '~> 3.11', require: false
  gem 'capistrano-rbenv', '~> 2.1'
  gem 'capistrano-rails', '~> 1.4'
  gem 'capistrano3-puma'
  gem 'capistrano-sidekiq'


end

group :development, :test do
  gem 'rspec-rails'
  gem 'faker'
  gem 'timecop'
  gem 'byebug'
end

group :test do
  gem 'factory_girl_rails'
  gem 'rspec-sidekiq', '~> 2.0'
  gem 'simplecov', require: false
  gem 'simplecov-html', require: false
  gem 'webmock'
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
gem 'jbuilder'


# gema con un cache lru
gem 'lru_redux'

# monitoreo de actividad
gem "audited-activerecord", "~> 3.0"

# gema para subir SFTP para chilena
gem "net-sftp"

# To use debugger
# gem 'debugger'


# vamos a monitorear el servicio
gem 'newrelic_rpm'
# gem 'appsignal'
# y tambien los errores
gem 'rollbar'



# gema para facilitar los request
gem 'rest-client'

# y para SOAP
gem 'savon', '~> 2.0'

# crea uuid unicos
gem 'uuid'

# paginacion
gem 'kaminari', '0.17.0'

# manejo de imagenes
gem 'mini_magick'

# manejo de codigos de barra
gem 'rqrcode' # esta es dependencia de barby
gem 'barby'

# nueva gema de permisos
gem 'cancancan', '~> 1.10'

# gema para geolocalizacion
gem 'geocoder'
